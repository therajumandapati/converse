﻿namespace Converse.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using Domain;
    using ServiceStack.Redis;

    [EnableCors(origins: "*", headers:"*", methods: "*")]
    public class ConversationController : ApiController
    {
        private readonly string _redisCacheUrl = ConfigurationManager.ConnectionStrings["RedisCache"].ConnectionString;

        //public async Task<Conversation> Get(Guid id)
        //{
        //    using (var client = new RedisClient(_redisCacheUrl))
        //    {
        //        var conversationRepository = client.As<Conversation>();
        //        return conversationRepository.GetById(id.ToString());
        //    }
        //}

        [HttpGet]
        public async Task<IEnumerable<Conversation>> Get(Guid userId)
        {
            using (var client = new RedisClient(_redisCacheUrl))
            {
                var conversationRepository = client.As<Conversation>();
                return conversationRepository.GetAll()
                    .Where(x => x.Participants != null && x.Participants.Contains(userId))
                    .OrderByDescending(x=> x.CreatedAt);
            }
        }

        [HttpPost]
        public async Task<Guid> Post(Conversation conversation)
        {
            using (var client = new RedisClient(_redisCacheUrl))
            {
                var conversationRepository = client.As<Conversation>();
                conversation.Id = Guid.NewGuid();
                conversation.CreatedAt = DateTime.Now;
                conversationRepository.Store(conversation);
                return conversation.Id;
            }
        }
    }
}
