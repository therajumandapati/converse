﻿namespace Converse.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Domain;
    using ServiceStack.Redis;

    public class UserController : ApiController
    {
        private readonly string _redisCacheUrl = ConfigurationManager.ConnectionStrings["RedisCache"].ConnectionString;

        public async Task<User> Get(Guid id)
        {
            using (var client = new RedisClient(_redisCacheUrl))
            {
                var conversationRepository = client.As<User>();
                return conversationRepository.GetById(id.ToString());
            }
        }

        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            using (var client = new RedisClient(_redisCacheUrl))
            {
                var conversationRepository = client.As<User>();
                return conversationRepository.GetAll();
            }
        }

        [HttpPost]
        public async Task<Guid> Post(User user)
        {
            using (var client = new RedisClient(_redisCacheUrl))
            {
                var conversationRepository = client.As<User>();
                user.Id = Guid.NewGuid();
                conversationRepository.Store(user);
                return user.Id;
            }
        }
    }
}
