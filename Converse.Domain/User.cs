﻿namespace Converse.Domain
{
    using System;

    public class User
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName
        {
            get { return string.Concat(FirstName, LastName); }
        }
    }
}
