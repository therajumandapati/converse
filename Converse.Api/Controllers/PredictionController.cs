﻿namespace Converse.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using Domain;
    using ServiceStack.Redis;

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PredictionController : ApiController
    {
        private readonly string _redisCacheUrl = ConfigurationManager.ConnectionStrings["RedisCache"].ConnectionString;

        [HttpGet]
        public IEnumerable<User> Get(string query)
        {
            if (string.IsNullOrWhiteSpace(query)) return null;
            using (var client = new RedisClient(_redisCacheUrl))
            {
                var userRepository = client.As<User>();
                //return userRepository.GetAll().Where(x => x.FullName.ToLower().Contains(query.ToLower()));
                return
                    userRepository.GetAll()
                        .Where(x => x.FullName.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) != -1);
            }
        }
    }
}
