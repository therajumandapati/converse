﻿namespace Converse.Domain
{
    public enum PredictionType
    {
        User,
        Date,
        DateTime,
        Word
    }
}