﻿namespace Converse.Domain
{
    public class Prediction<T>
    {
        public PredictionType Type { get; set; }
        public T Value { get; set; }
    }
}
