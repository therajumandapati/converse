﻿namespace Converse.Api
{
    using System;
    using System.Configuration;
    using Microsoft.Owin.Hosting;

    public static class ApiProgram
    {
        public static void Main()
        {
            var options = new StartOptions();
            var urlsString = ConfigurationManager.AppSettings["ListeningUrls"];
            var urls = urlsString.Split(';');
            foreach (var url in urls)
            {
                options.Urls.Add(url);
            }

            var port = ConfigurationManager.AppSettings["ListeningPort"];
            options.Urls.Add(string.Format("http://{0}:{1}", Environment.MachineName, port));

            Console.WriteLine("Starting web Server...");
            WebApp.Start<Startup>(options);
            Console.WriteLine("Server running at port {0} - press Enter to quit. ", 9000);
            Console.ReadLine();
        } 
    }
}
