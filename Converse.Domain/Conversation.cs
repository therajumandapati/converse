﻿namespace Converse.Domain
{
    using System;
    using System.Collections.Generic;

    public class Conversation
    {
        public Guid Id { get; set; }
        public Conversation ParentConversation { get; set; }
        public IEnumerable<Guid> Participants { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Text { get; set; }
    }
}
